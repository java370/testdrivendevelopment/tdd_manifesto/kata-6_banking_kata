package versionTwo;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class AccountTest {
    @Mock
    private Account account;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void WhenDepositBalanceShouldIncrease() {
        int amount = 200;
        doNothing().when(account).deposit(amount);
        account.deposit(amount);
        verify(account, times(1)).deposit(amount);
    }

    @Test
    void WhenWithdrawBalanceShouldDecrease() {
        int amount = 200;
        doNothing().when(account).withdraw(amount);
        account.withdraw(amount);
        verify(account, times(1)).withdraw(amount);
    }
}
